package com.abn.intg.test;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.abn.intg.model.Recipe;
import com.abn.intg.service.RecipeService;



@SpringBootTest
@ActiveProfiles("int")
public class RecipeServiceUnitTest {
	
	@Autowired
    private RecipeService recipeService;
	
	
    @Test
	public void searchAllRecipes() throws Exception {
    	    	
    	List<Recipe> result = recipeService.findAllRecipes("");    	
    	assertThat(result.size()).isEqualTo(6);
		
	}
    
    
    @Test
	public void searchAllVegetarianRecipes() throws Exception {
    	    	
    	List<Recipe> result = recipeService.findAllRecipes("type:Vegetarian");    	
    	assertThat(result.size()).isEqualTo(3);
		
	}
        
    @Test
	public void searchAllRecipes_WithIngredientAsPotatoesAnd4Servings() throws Exception {
    	    	
    	List<Recipe> result = recipeService.findAllRecipes("ingredients~potatoes,servings:4");    	
    	assertThat(result.size()).isEqualTo(2);
		
	}
    
    
    

}
