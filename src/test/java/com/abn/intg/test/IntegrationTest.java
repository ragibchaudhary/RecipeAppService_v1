package com.abn.intg.test;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("int")
public class IntegrationTest {
	
	@Autowired
	private MockMvc mockMvc;


	@Test
	public void addRecipe() throws Exception {
		this.mockMvc.perform(post("/RecipeApp/v1/recipes").content("[{\"name\":\"recipe7\",\"type\":\"Vegetarian\",\"servings\":3,\"ingredients\" : \"spinach,tomato,oil,salt\",\"instructions\": \"these are the instructions for recioe 7\"}]")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isCreated());
	}
	
	
	@Test
	public void findAllRecipes() throws Exception {
		this.mockMvc.perform(get("/RecipeApp/v1/recipes")).andDo(print()).andExpect(status().isOk())
		.andExpect(content().string(containsString("Recipe1")))
		.andExpect(content().string(containsString("Recipe2")))
		.andExpect(content().string(containsString("Recipe4")))
		.andExpect(content().string(containsString("Recipe6")));
	}
	
		
	@Test
	public void findRecipes_AllVegetarian() throws Exception {
		this.mockMvc.perform(get("/RecipeApp/v1/recipes?filter=type:Vegetarian")).andDo(print()).andExpect(status().isOk())
		.andExpect(content().string(containsString("Recipe1")))
		.andExpect(content().string(containsString("Recipe2")))
		.andExpect(content().string(containsString("Recipe6")));
	}

	
	@Test
	public void findRecipes_WithIngredientAsPotatoesAnd4Servings() throws Exception {
		this.mockMvc.perform(get("/RecipeApp/v1/recipes?filter=ingredients~potatoes,servings:4")).andDo(print()).andExpect(status().isOk())
		.andExpect(content().string(containsString("Recipe2")))
		.andExpect(content().string(containsString("Recipe4")));
	}
	
	
	@Test
	public void findRecipes_WithoutIngredientAsSalmonAndInstructionsHasOven() throws Exception {
		this.mockMvc.perform(get("/RecipeApp/v1/recipes?filter=ingredients!~salmon,instructions~oven")).andDo(print()).andExpect(status().isOk())
		.andExpect(content().string(containsString("Recipe1")))
		.andExpect(content().string(containsString("Recipe4")));
	}
	
	
	@Test
	public void updateRecipe() throws Exception {
		this.mockMvc.perform(put("/RecipeApp/v1/recipes/5").content("{\"name\":\"recipe5_updated\"}")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("recipe5_updated")));
	}
	
	
	@Test
	public void deleteRecipe() throws Exception {
		this.mockMvc.perform(delete("/RecipeApp/v1/recipes/3"))
				.andDo(print()).andExpect(status().isOk());
	}
	
	
}
