package com.abn.intg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abn.intg.model.Recipe;
import com.abn.intg.service.RecipeService;

@RestController
@RequestMapping(value = "/RecipeApp/v1")
public class RecipeController {
	
	@Autowired
    private RecipeService service;
	
	
	/**
	 * This method exposes a HTTP POST endpoint to insert recipes
	 * into database. It takes list of recipes as an input.
	 * 
	 * @param recipies
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/recipes")
	@ResponseStatus(HttpStatus.CREATED)
    public void createRecipe(@RequestBody List<Recipe> recipies) {
		recipies.forEach(recipe -> service.createRecipe(recipe));
        
    }
	
	
	/**
	 * This method exposes a HTTP GET endpoint to fetch recipes
	 * from database based on the filter criteria specified in
	 * URL as query parameter
	 * 
	 * @param filterCondtion
	 * 
	 * @return List<Recipe>
	 */
	
	@RequestMapping(method = RequestMethod.GET, value = "/recipes")
    @ResponseBody
    public ResponseEntity<List<Recipe>> searchRecipes(@RequestParam(value = "filter",required=false) String filterCondtion) {		
        return  new ResponseEntity<>(service.findAllRecipes(filterCondtion), HttpStatus.OK);
    }	
	
	
	
	/**
	 * This method exposes a HTTP PUT endpoint to update recipe
	 * based on id present in URL as path parameter
	 * 
	 * @param recipe
	 * 
	 * @param id
	 * 
	 * @return <Recipe>
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/recipes/{id}")
    public ResponseEntity<Recipe> updateRecipe(@RequestBody Recipe recipe, @PathVariable("id") Long id) {
        return new ResponseEntity<>(service.updateRecipe(recipe, id), HttpStatus.OK);
    }
	
	
	/**
	 * * This method exposes a HTTP DELETE endpoint to delete recipe
	 * based on id present in URL as path parameter
	 * 
	 * @param id
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/recipes/{id}")
	@ResponseStatus(HttpStatus.OK)
    public void deleteRecipe(@PathVariable("id") Long id) {
		service.deleteRecipe(id);
    }
	

}
