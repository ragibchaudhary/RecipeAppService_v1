package com.abn.intg.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/*POJO to define the structure of a Recipe entity*/
@Entity
public class Recipe {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 private Long id;
	 
	 private String name;
	 private String type;
	 private int servings;
	 private String ingredients;
	 private String instructions;
	 
	 
	 
	
	public Recipe(Long id, String name, String type, int servings, String ingredients, String instructions) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.servings = servings;
		this.ingredients = ingredients;
		this.instructions = instructions;
	}
	
	public Recipe() {
		
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getServings() {
		return servings;
	}
	public void setServings(int servings) {
		this.servings = servings;
	}
	public String getIngredients() {
		return ingredients;
	}
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	 
	 
	 

}
