package com.abn.intg;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.abn.intg.model.Recipe;
import com.abn.intg.repo.RecipeRepository;


/**
 * @author Raghib Ali
 *
 */
@SpringBootApplication
public class RecipeAppServiceBoot implements CommandLineRunner {
	
	@Autowired(required=false)
    private RecipeRepository recipeRepo;
	
    public static void main( String[] args )
    {
    	SpringApplication.run(RecipeAppServiceBoot.class, args);
    }
    
    
    
    /**
	 * This method is invoked during the service startup. 
	 * It loads the recipes data into the in-memory database configured in application properties
	 *
	 * @param args
	 *          argument passed from command line during service startup
	 * 
	 * @throws Exception
	 *             if any exception occurs during data load.
	 * 
	 */

    @Override
    public void run(String... args) throws Exception {
    	List<Recipe> recipies = Arrays.asList(
                new Recipe(1L,"Recipe1", "Vegetarian", 2,"spinach,onion,oil,salt","instructions for recipe 1. cook in the oven."),
                new Recipe(2L,"Recipe2", "Vegetarian", 4,"carrot,onion,potatoes,oil,salt","instructions for recipe 2"),
                new Recipe(3L,"Recipe3", "Non-Vegetarian", 4,"salmon,carrot,onion,oil,salt","instructions for recipe 3. cook in oven"),
                new Recipe(4L,"Recipe4", "Non-Vegetarian", 4,"chicken,potatoes,onion,oil,salt","instructions for recipe 4. cook in oven"),
                new Recipe(5L,"Recipe5", "Non-Vegetarian", 3,"salmon,potatoes,onion,oil,salt","instructions for recipe 5"),
                new Recipe(6L,"Recipe6", "Vegetarian", 5,"potatoes,onion,oil,salt","instructions for recipe 6")
            
        );
    	
    	recipeRepo.saveAll(recipies);
    }
}
