package com.abn.intg.service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.abn.intg.jpa.FilterCondition;
import com.abn.intg.jpa.FilterOperationEnum;
import com.abn.intg.jpa.RecipeSpecificationBuilder;
import com.abn.intg.model.Recipe;
import com.abn.intg.repo.RecipeRepository;


@Service
public class RecipeService {
	
	@Autowired
    private RecipeRepository recipeRepo;
		
	
	
	public void createRecipe(Recipe recipe) {
        recipeRepo.save(recipe);
    }
	
	/*This method parses the filter criteria
	 *and call other methods to map those filters into JPA Specifications.
	 * It finally executes the search request to JPA repository to fetch the desired result.
	 */
	public List<Recipe> findAllRecipes(String searchCriteria) {	
		if(searchCriteria!=null && !searchCriteria.isEmpty()){
			RecipeSpecificationBuilder builder = new RecipeSpecificationBuilder();
			Pattern pattern = Pattern.compile("(\\p{Punct}?)(\\w+?)(" + FilterOperationEnum.OPERATION_SET + ")(\\w+?),");
	        Matcher matcher = pattern.matcher(searchCriteria + ",");
	        while (matcher.find()) {
	            builder.addFilter(new FilterCondition(matcher.group(1), matcher.group(2), matcher.group(3),matcher.group(4))); //creates a list of filters
	        }

	        Specification<Recipe> spec = builder.build(); //map filters to specifications and creates final specification. 
	        return recipeRepo.findAll(spec);
		}else{
			return recipeRepo.findAll();
		}
		
    }
	
		
	public Recipe updateRecipe(Recipe newRecipe, Long id) {
		
        Optional<Recipe> oldRecipe = recipeRepo.findById(id);
                
        if(oldRecipe.isPresent()) 
        {
            if(newRecipe.getIngredients()!=null) oldRecipe.get().setIngredients(newRecipe.getIngredients());
            if(newRecipe.getInstructions()!=null) oldRecipe.get().setInstructions(newRecipe.getInstructions());
            if(newRecipe.getName()!=null) oldRecipe.get().setName(newRecipe.getName());
            if(newRecipe.getServings()!=0) oldRecipe.get().setServings(newRecipe.getServings());
            if(newRecipe.getType()!=null) oldRecipe.get().setType(newRecipe.getType());
            newRecipe = recipeRepo.save(oldRecipe.get());
             
            return newRecipe;
        } else {
        	newRecipe = recipeRepo.save(newRecipe);
             
            return newRecipe;
        }
    }
	
	
	public void deleteRecipe(Long id) {
        recipeRepo.deleteById(id);
    }	
	

}
