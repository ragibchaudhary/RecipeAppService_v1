package com.abn.intg.jpa;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.abn.intg.model.Recipe;

/*This class basically create Specifications based on the filter criteria 
 * and combines those Specifications to create a final Specification.
 * It combines the Specifications with AND/OR operator 
 * as specified in filter criteria.
 * 
 */

public class RecipeSpecificationBuilder {

	 private final List<FilterCondition> filters;

	 public RecipeSpecificationBuilder() {
		 filters = new ArrayList<>();
	 }
	 
	 /*This method creates an list of filters as specified in search request*/
	 public final RecipeSpecificationBuilder addFilter(FilterCondition filter) {
		 filters.add(filter);
	     return this;
	 }
	 
	 /*This method combines the Specifications with AND/OR operator and returns a final Specification */
	 public Specification<Recipe> build() {
	        if (filters.size() == 0)
	            return null;

	        Specification<Recipe> finalSpefication = new RecipeSpecification(filters.get(0));
	     
	        for (int i = 1; i < filters.size(); i++) {
	        	
	        		
	        	finalSpefication = filters.get(i).isOrCondition()
	              ? Specification.where(finalSpefication).or(new RecipeSpecification(filters.get(i))) 
	              : Specification.where(finalSpefication).and(new RecipeSpecification(filters.get(i)));           
	              
	        }
	        
	        return finalSpefication;
	 }
	    
}
