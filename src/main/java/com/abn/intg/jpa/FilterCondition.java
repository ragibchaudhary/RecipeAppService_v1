package com.abn.intg.jpa;

/*A Bean class to define the structure of filter condition provided in the search requests*/
public class FilterCondition {
	
	private String filterKey;
    private Object value;
    private String operation;
    private boolean orCondition;

    public FilterCondition(String filterKey, String operation, Object value){
        super();
        this.filterKey = filterKey;
        this.operation = operation;
        this.value = value;
    }
    
    public FilterCondition(String orPredicate, String filterKey, String operation, Object value) {
        super();
        this.orCondition = orPredicate != null && orPredicate.equals(FilterOperationEnum.OR_CONDITON_FLAG);
        this.filterKey = filterKey;
        this.operation = operation;
        this.value = value;
    }

	public String getFilterKey() {
		return filterKey;
	}

	public void setFilterKey(String filterKey) {
		this.filterKey = filterKey;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public boolean isOrCondition() {
		return orCondition;
	}

	public void setOrCondition(boolean orCondition) {
		this.orCondition = orCondition;
	}

    
    

}
