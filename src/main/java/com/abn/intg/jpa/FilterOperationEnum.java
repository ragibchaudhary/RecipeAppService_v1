package com.abn.intg.jpa;

/*This enum provides the list of operations supported by search request.*/
public enum FilterOperationEnum {
	
	EQUAL,
    NOT_EQUAL,
	CONTAINS,
	NOT_CONTAINS,
	GREATER_THAN,
	GREATER_THAN_EQUAL,
    LESS_THAN,
    LESS_THAN_EQUAL;

    public static final String OPERATION_SET = ":|!:|~|!~|>|>=|<|<=";
    
    public static final String OR_CONDITON_FLAG = "'";

    /*This method translates the operator symbol to Simple Operation literals.*/
    public static FilterOperationEnum getOperation(String input) {
        switch (input) {
        case ":":
            return EQUAL;
        case "!:":
            return NOT_EQUAL;       
        case "~":
            return CONTAINS;
        case "!~":
            return NOT_CONTAINS;
        case ">":
            return GREATER_THAN;
        case ">=":
            return GREATER_THAN_EQUAL;
        case "<":
            return LESS_THAN;
        case "<=":
            return LESS_THAN_EQUAL;
        default:
            return null;
        }
    }
}
