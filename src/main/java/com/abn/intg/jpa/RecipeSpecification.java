package com.abn.intg.jpa;

import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.abn.intg.model.Recipe;

/*This class creates the JPA Specification based on the filter criteria passed as an constructor parameter. 
 * 
 * Basically it builds the predicates based on the filter condition automatically 
 * 
 * without having the need to initialize  Root, CriteriaQuery and CriteriaBuilder. 
 *
*/

public class RecipeSpecification implements Specification<Recipe>{
	
	private final FilterCondition searchFilter;

    public RecipeSpecification(final FilterCondition searchFilter){
        super();
        this.searchFilter = searchFilter;
    }

    
    /*This method is used to build predicates based on the filter criteria provided in the search request. */
    @Override
	public Predicate toPredicate(Root<Recipe> root, CriteriaQuery<?> crQry, CriteriaBuilder crBdr) {
		
		String filterKey = searchFilter.getFilterKey();
		String filterValue = searchFilter.getValue().toString();

        switch(Objects.requireNonNull(FilterOperationEnum.getOperation(searchFilter.getOperation()))){
            case CONTAINS:             
                return crBdr.like(root.get(filterKey), "%" + filterValue + "%");

            case NOT_CONTAINS:            
                return crBdr.notLike(root.get(filterKey), "%" + filterValue + "%");

            case EQUAL:
                return crBdr.equal(root.get(filterKey), filterValue);

            case NOT_EQUAL:
               return crBdr.notEqual(root.get(filterKey), filterValue);

            case GREATER_THAN:
                return crBdr.greaterThan(root.<String> get(filterKey), filterValue);

            case GREATER_THAN_EQUAL:
                return crBdr.greaterThanOrEqualTo(root.<String> get(filterKey), filterValue);

            case LESS_THAN:
                return crBdr.lessThan(root.<String> get(filterKey), filterValue);

            case LESS_THAN_EQUAL:
                return crBdr.lessThanOrEqualTo(root.<String> get(filterKey), filterValue);
            default:
    			return null;
        }

        
	}
	
	
}
