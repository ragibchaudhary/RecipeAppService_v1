# RecipeAppService_v1

Java REST Application based on Spring Boot framework to manage recipes


## Description

The Recipe management REST application is based on Java Spring Boot framework. It offers all the CRUD operations to manage the recipes stored in a persistance database. For the sake of demonstration we have employed an in-memory H2 database which is configured automatically at the application startup.


## How to run the application?
**1. What you need:**
- Computer or a laptop
- Java 8+ installed on your system

**2. Steps:**
- Download the jar from the gitlab repository at [RecipeAppService_v1-8.0.0.jar](https://gitlab.com/ragibchaudhary/RecipeAppService_v1/-/blob/main/RecipeAppService_v1-8.0.0.jar)
- Open the command prompt on your system and navigate to the directory where jar file is downloaded.
- Run the following command in command prompt:
`java -jar RecipeAppService_v1-8.0.0.jar`
- Open your browser and enter the URL: [http://localhost:9888/RecipeApp/v1/recipes](http://localhost:9888/RecipeApp/v1/recipes)

Note: The database can be accessed using the URL: [http://localhost:9888/h2-console](http://localhost:9888/h2-console)



## Operations

The technical specification of an application is available on swagger hub at: [RecipeAppService_v1](https://app.swaggerhub.com/apis-docs/user_77/RecipeAppService_v1/1.0#/)

**1. Add a new recipes (HTTP POST):**

This feature creates a new recipe to a database and returns a successful response on completion. For more info, click [here](https://app.swaggerhub.com/apis-docs/user_77/RecipeAppService_v1/1.0#/RecipeController/createRecipeUsingPOST)

**2. Get recipes (HTTP GET):**

This feature is used to fetch all the recipes or a subset of recipes from the database based on a filter criteria specified in the request URL. 
The format of the filter criteria is provided below along with few examples.

Only one filter: `http://localhost:9888/RecipeApp/v1/recipes?filter={key}{operator}{value}`

Multiple filters with AND: `http://localhost:9888/RecipeApp/v1/recipes?filter={key}{operator}{value},{key}{operator}{value}`

Multiple filters with OR: `http://localhost:9888/RecipeApp/v1/recipes?filter={key}{operator}{value},'{key}{operator}{value}`

| {operator} | Meaning |
| ------ | ------ |
| : | EQUAL |
| !: | NOT EQUAL |
| ~ | CONTAINS|
| !~ | NOT_CONTAINS |
| > | LESS_THAN |
| !: | LESS_THAN_EQUAL  |
| !: | GREATER_THAN |
| !: | GREATER_THAN_EQUAL |

Examples:

`http://localhost:9888/RecipeApp/v1/recipes?filter=type:Vegetarian`   --> Find all recipes where _type equals vegetarian_

`http://localhost:9888/RecipeApp/v1/recipes?filter=ingredients!~Salmon`   --> Find all recipes without _ingredients as Salmon_

For more info, click [here](https://app.swaggerhub.com/apis-docs/user_77/RecipeAppService_v1/1.0#/RecipeController/searchRecipesUsingGET)


**3. Update a recipe (HTTP PUT)**

This feature updates an existing recipe based on {id} or creates a new recipe if it doesn't exist. For more info, click [here](https://app.swaggerhub.com/apis-docs/user_77/RecipeAppService_v1/1.0#/RecipeController/updateRecipeUsingPUT)

**4. Delete a recipe (HTTP DELETE)**

This feature deletes an existing recipe based on {id}. For more info, click [here](https://app.swaggerhub.com/apis-docs/user_77/RecipeAppService_v1/1.0#/RecipeController/deleteRecipeUsingDELETE)


## Support
For any queries, reach out to me at ragibchaudhary@gmail.com


## Authors
Raghib Ali
